﻿#if !NO_RUNTIME
using System;
using ProtoBuf.Meta;

#if FEAT_IKVM
using Type = IKVM.Reflection.Type;
using IKVM.Reflection;
#else
using System.Reflection;
#endif

namespace ProtoBuf.Serializers
{
    sealed class TupleSerializer : IProtoTypeSerializer
    {
        private readonly MemberInfo[] members;
        private readonly ConstructorInfo ctor;
        private IProtoSerializer[] tails;
        public TupleSerializer(RuntimeTypeModel model, ConstructorInfo ctor, MemberInfo[] members)
        {
            if (ctor == null) throw new ArgumentNullException("ctor");
            if (members == null) throw new ArgumentNullException("members");
            this.ctor = ctor;
            this.members = members;
            this.tails = new IProtoSerializer[members.Length];

            ParameterInfo[] parameters = ctor.GetParameters();
            for (int i = 0; i < members.Length; i++)
            {
                WireType wireType;
                Type finalType = parameters[i].ParameterType;

                Type itemType = null, defaultType = null;

                MetaType.ResolveListTypes(model, finalType, ref itemType, ref defaultType);
                Type tmp = itemType == null ? finalType : itemType;

                bool asReference = false;
                int typeIndex = model.FindOrAddAuto(tmp, false, true, false);
                if (typeIndex >= 0)
                {
                    asReference = model[tmp].AsReferenceDefault;
                }
                IProtoSerializer tail = ValueMember.TryGetCoreSerializer(model, DataFormat.Default, tmp, out wireType, asReference, false, false, true), serializer;
                if (tail == null)
                {
                    throw new InvalidOperationException("No serializer defined for type: " + tmp.FullName);
                }

                tail = new TagDecorator(i + 1, wireType, false, tail);
                if (itemType == null)
                {
                    serializer = tail;
                }
                else
                {
                    if (finalType.IsArray)
                    {
                        serializer = new ArrayDecorator(model, tail, i + 1, false, wireType, finalType, false, false);
                    }
                    else
                    {
                        serializer = ListDecorator.Create(model, finalType, defaultType, tail, i + 1, false, wireType, true, false, false);
                    }
                }
                tails[i] = serializer;
            }
        }
        public bool HasCallbacks(Meta.TypeModel.CallbackType callbackType)
        {
            return false;
        }

#if FEAT_COMPILER
        public void EmitCallback(Compiler.CompilerContext ctx, Compiler.Local valueFrom, Meta.TypeModel.CallbackType callbackType) { }
#endif
        public Type ExpectedType
        {
            get { return ctor.DeclaringType; }
        }



#if !FEAT_IKVM
        void IProtoTypeSerializer.Callback(object value, Meta.TypeModel.CallbackType callbackType, SerializationContext context) { }
        object IProtoTypeSerializer.CreateInstance(ProtoReader source) { throw new NotSupportedException(); }
        private object GetValue(object obj, int index)
        {
            PropertyInfo prop;
            FieldInfo field;

            if ((prop = members[index] as PropertyInfo) != null)
            {
                if (obj == null)
                    return Helpers.IsValueType(prop.PropertyType) ? Activator.CreateInstance(prop.PropertyType) : null;
                return prop.GetValue(obj, null);
            }
            else if ((field = members[index] as FieldInfo) != null)
            {
                if (obj == null)
                    return Helpers.IsValueType(field.FieldType) ? Activator.CreateInstance(field.FieldType) : null;
                return field.GetValue(obj);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
        public object Read(object value, ProtoReader source)
        {
            object[] values = new object[members.Length];
            bool invokeCtor = false;
            if (value == null)
            {
                invokeCtor = true;
            }
            for (int i = 0; i < values.Length; i++)
                values[i] = GetValue(value, i);
            int field;
            while ((field = source.ReadFieldHeader()) > 0)
            {
                invokeCtor = true;
                if (field <= tails.Length)
                {
                    IProtoSerializer tail = tails[field - 1];
                    values[field - 1] = tails[field - 1].Read(tail.RequiresOldValue ? values[field - 1] : null, source);
                }
                else
                {
                    source.SkipField();
                }
            }
            return invokeCtor ? ctor.Invoke(values) : value;
        }
        public void Write(object value, ProtoWriter dest)
        {
            for (int i = 0; i < tails.Length; i++)
            {
                object val = GetValue(value, i);
                if (val != null) tails[i].Write(val, dest);
            }
        }
#endif
        public bool RequiresOldValue
        {
            get { return true; }
        }

        public bool ReturnsValue
        {
            get { return false; }
        }
        Type GetMemberType(int index)
        {
            Type result = Helpers.GetMemberType(members[index]);
            if (result == null) throw new InvalidOperationException();
            return result;
        }
        bool IProtoTypeSerializer.CanCreateInstance() { return false; }
    }
}

#endif