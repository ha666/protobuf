﻿namespace Protobuf.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] bytes;
            Trade trade = new Trade();
            trade.Tid = 9223372036854775806;
            trade.TotalFee = 3.50M.ToString("F2");
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                ProtoBuf.Meta.RuntimeTypeModel.Default.Serialize(stream, trade);
                bytes = stream.ToArray();
            }
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream(bytes))
            {
                Trade trade0 = ProtoBuf.Meta.RuntimeTypeModel.Default.Deserialize(stream, null, typeof(Trade)) as Trade;
            }
        }
    }
}
