﻿using ProtoBuf;

namespace Protobuf.Example
{
    [ProtoContract]
    public class Trade
    {

        [ProtoMember(1)]
        public long Tid { get; set; }

        [ProtoMember(2)]
        public string TotalFee { get; set; }

    }
}
